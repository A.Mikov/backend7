<!DOCTYPE html>
<html lang="ru">


	<head>
      <meta charset="utf-8"/>
       <title>Kikoriki</title>
       <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
       <link href="style.css" rel="stylesheet">
       <link href="https://c7.hotpng.com/preview/7/64/896/computer-icons-shutdown-button-logo-power.jpg" rel="icon">
  </head>

	<body>
	<div class="container-lg px-0">	
  <div class="main row mx-auto" style="flex-direction:column;">
<?php
  if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW'])) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Enter login and password"');
    print('<h1>401 Требуется авторизация</h1></div></body>');
    exit();
  }

  $user = 'u36993';
  $pass = '3932198';
  $db = new PDO('mysql:host=localhost;dbname=u36993', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
  $login = trim($_SERVER['PHP_AUTH_USER']);
  $pass_hash = substr(hash("sha256", trim($_SERVER['PHP_AUTH_PW'])), 0, 20);
  $stmtCheck = $db->prepare('SELECT password_admin FROM admin WHERE login_admin = ?');
  $stmtCheck->execute([$login]);
  $row = $stmtCheck->fetch(PDO::FETCH_ASSOC);
  
  if ($row == false || $row['password_admin'] != $pass_hash) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Invalid login or password"');
    print('<h1>401 Неверный логин или пароль</h1>');
    exit();
  }
?>
    <section>
        <h2 style="text-align:center;">Администрирование</h2>
    </section>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // пароль qwerty
  
  // $stmtCount = $db->prepare('SELECT supers, count(fs.user_id) AS amount FROM abil_form AS sp LEFT JOIN user_sups AS fs ON sp.id_sup = fs.id_sup GROUP BY sp.id_sup');
  // $stmtCount->execute();
  // print('<div>');
  // while($row = $stmtCount->fetch(PDO::FETCH_ASSOC)) {
  //     print('<b>' . $row['supers'] . '</b>: ' . $row['amount'] . '<br/>'); 
  // }
  // print('</div>');

  $stmt1 = $db->prepare('SELECT  user_id, name, email, bd, pol, kon, bio, login FROM FORM');
  $stmt2 = $db->prepare('SELECT id_sup FROM user_sups WHERE user_id = ?');
  $stmt1->execute();

  while($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      print('<div style ="text-align:center; margin-top:25px;">');
      print('<h2>' . $row['login'] . '</h2>');
      $superpowers = [false, false, false, false];
      $stmt2->execute([$row['user_id']]);
      while ($superrow = $stmt2->fetch(PDO::FETCH_ASSOC)) {
          $superpowers[$superrow['id_sup'] - 1] = true;
      }
      include('adminform.php');
      print('</div>');
  }
} else {
  if (array_key_exists('delete', $_POST)) {
    $user = 'u36993';
    $pass = '3932198';
    $db = new PDO('mysql:host=localhost;dbname=u36993', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('DELETE FROM user_sups WHERE  user_id = ?');
    $stmt1->execute([$_POST['uid']]);
    $stmt2 = $db->prepare('DELETE FROM  FORM WHERE user_id = ?');
    $stmt2->execute([$_POST['uid']]);
    header('Location: admin.php');
    exit();
  }
  $errors = FALSE;
  $trimed = [];
  $values=[];
  foreach ($_POST as $key => $value)
    if (is_string($value))
      $trimed[$key] = trim($value);
    else
      $trimed[$key] = $value;

  if (empty($trimed['name'])) {
    $errors = TRUE;
  }
  $values['name'] = $trimed['name'];

  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimed['email'])) {
    $errors = TRUE;
  }
  $values['email'] = $trimed['email'];

  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimed['bd'])) {
    $errors = TRUE;
  }
  $values['bd'] = $trimed['bd'];

  if (!preg_match('/^[MF]$/', $trimed['pol'])) {
    $errors = TRUE;
  }
  $values['pol'] = $trimed['pol'];

  if (!preg_match('/^[0-6]$/', $trimed['kon'])) {
    $errors = TRUE;
  }
  $values['kon'] = $trimed['kon'];

  foreach (['1', '2', '3', '4'] as $value) {
    $values['superpowers'][$value] = FALSE;
  }
  if (array_key_exists('superpowers', $trimed)) {
    foreach ($trimed['superpowers'] as $value) {
      print_r($value);
      if (!preg_match('/[1-4]/', $value)) {
        $errors = TRUE;
      }
      $values['superpowers'][$value] = TRUE;
    }
  }
  $values['bio'] = $trimed['bio'];
  

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    print('ERROR');
    //header('Location: admin.php');
    exit();
  }

  $user = 'u36993';
  $pass = '3932198';
  $db = new PDO('mysql:host=localhost;dbname=u36993', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt1 = $db->prepare('UPDATE FORM SET name=?, email=?, bd=?, pol=?, kon=?, bio=? WHERE user_id = ?');
  $stmt1->execute([$values['name'], $values['email'], $values['bd'], $values['pol'], $values['kon'], $values['bio'], $_POST['uid']]);

  $stmt2 = $db->prepare('DELETE FROM  user_sups WHERE user_id = ?');
  $stmt2->execute([$_POST['uid']]);

  $stmt3 = $db->prepare("INSERT INTO user_sups SET user_id = ?, id_sup = ?");
  foreach ($trimmedPost['superpowers'] as $s)
    $stmt3 -> execute([$_POST['uid'], $s]);

  header('Location: admin.php');
  exit();
}
?>
</div>
<nav id="navi">
        <ul>
          <li> <a href="../backend6" class="gradient-button" style="margin-top:15px;" title="Назад">На главную</a></li>
        </ul>
      </nav>
</div>
<footer id="footer_of_my_site">
			<b>Вы добрались до конца Internet</b>
    </footer>
</body>

