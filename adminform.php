<form action="admin.php"
        method="POST">
<input type="hidden" name="uid" value="<?php print $row['user_id']; ?>" />
<label>
    Имя<br />
    <input name="name" value="<?php print $row['name']; ?>"/>
</label><br />

<label>
    E-mail:<br />
    <input name="email" value="<?php print $row['email']; ?>" type="email" />
</label><br />

<label>
    Дата рождения:<br />
    <input name="bd" value="<?php print $row['bd']; ?>" type="date" />
</label><br />

Пол:<br />
<label class="labelRadio" for="rdo1">
            <input type="radio" id="rdo1" name="pol" value="M" <?php if ($row['pol'] == 'M') {print 'checked';} ?>>
            <span class="rdo"></span>
            <span>Муж</span>
          </label>          
          <label class="labelRadio" for="rdo2">
            <input type="radio" id="rdo2" name="pol" value="F" <?php if ($row['pol'] == 'F') {print 'checked';} ?>>
            <span class="rdo"></span>
            <span>Жен</span>
          </label>
<br />

Количество конечностей:<br />

 <label class="labelRadio" for="rdo3">
            <input type="radio" id="rdo3" name="kon" value="3" <?php if ($row['kon'] == '3') {print 'checked';} ?>>
            <span class="rdo"></span>
            <span>3</span>
          </label>          
          <label class="labelRadio" for="rdo4">
            <input type="radio" id="rdo4" name="kon" value="4"<?php if ($row['kon'] == '4') {print 'checked';} ?> >
            <span class="rdo"></span>
            <span>4</span>
          </label>
          <label class="labelRadio" for="rdo6">
            <input type="radio" id="rdo6" name="kon" value="6" <?php if ($row['kon'] == '6') {print 'checked';} ?> >
            <span class="rdo"></span>
            <span>6(General Kenobi)</span>
          </label>
<br />

<label>
    Сверхспособности:
    <br />
    <div class="select select--multiple">            
        <select name="superpowers[]" id="multi-select" multiple>
            <option value="1" <?php if ($superpowers[0]) {print 'selected';} ?>>Бессмертие</option>
            <option value="2" <?php if ($superpowers[1]) {print 'selected';} ?>>Прохождение сквозь стены</option>
            <option value="3" <?php if ($superpowers[2]) {print 'selected';} ?>>Левитация</option>
            <option value="4" <?php if ($superpowers[3]) {print 'selected';} ?>>Телепортация</option>
        </select>
        <span class="focus"></span>
    </div>
</label><br />

<label>
    Биография:<br />
    <textarea name="bio"><?php print $row['bio']; ?></textarea>
</label><br />

<input id="submit" type="submit" value="Изменить" name="update" />
<input id="submit" type="submit" value="Удалить" name="delete"/>
</form>
